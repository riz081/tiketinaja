import React, { useEffect } from 'react'
import { SplashBackground } from '../../assets'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { Image } from 'native-base'


const Splash = ({ navigation }) => {    

    useEffect( () => {
        setTimeout ( () => {
            
            AsyncStorage.getItem('username').then((value) => {
                value === null ? (
                  navigation.replace('onBoarding')
                  ) : (
                    navigation.replace('MainApp')
                  )        
              })

        }, 3500)
    }, [navigation])

  return (
    
        <Image source={SplashBackground}
          alignItems = 'center'
          flex = {1}
          justifyContent = 'center'
        />
    
  )
}

export default Splash
