import React, { Component, useState } from 'react';
import { Box, Pressable, Text } from 'native-base';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';



const ItemView = ({navigation}) => {

    const [dataPesawat, setDataPesawat] = useState({
      iata : '',
      shortName : '',
  })

const optionsPesawat = {
  method: 'GET',
  headers: {
      'X-RapidAPI-Key': '0df8b82465msh5ec435d63e5d37ep1101f9jsnb1e132289fdc',
      'X-RapidAPI-Host': 'aerodatabox.p.rapidapi.com'
  }
};


const getDataPesawat = () => {
  fetch('https://aerodatabox.p.rapidapi.com/airports/icao/WIHH', optionsPesawat)
  .then(response => response.json())
  .then(json => {
      console.log(json);
      setDataPesawat(json)
  })
  .catch(err => console.error(err));
}

return (
  <Box>

    {/* Data Pesawat */}
    <Pressable
      onPress={() => navigation.navigate('ListDetail')}
    >
      
      <Box 
        borderWidth={'1.5px'}
        borderColor = {'#efeff0'}
        mb = {'12px'}
        p = {'20px'}
        borderRadius = {'12px'}
      >                    
          
          <Text 
            fontSize={'23px'}
            fontWeight = 'bold'
            color={'#585fcc'}
            mb={'10px'}
          >
              50000             
          </Text>

          <Text 
              fontSize={'15px'}
              fontWeight = 'bold'
              color={'#585fcc'}
              mb={'10px'}
            >
              Type :
            </Text>

            <Box flexDirection={'row'} justifyContent = 'space-between'>
              <Text 
                color={'#24333a'}
                fontSize = {'14px'}
                fontWeight = 'bold'
              >
                <FontAwesome name="plane" size={20}/> {'  '}
                {dataPesawat == null ?  "Tunggu" : dataPesawat.iata}
              </Text>

              <Text 
                color={'#24333a'}
                fontSize = {'14px'}
                fontWeight = 'bold'
              >
                <MaterialIcons name="local-airport" size={20}/> {'  '}
                {dataPesawat == null ?  "Tunggu" : dataPesawat.shortName}
              </Text>

              <Text 
                color={'#24333a'}
                fontSize = {'14px'}
                fontWeight = {'bold'}
              >
                <Ionicons name="ios-star" size={20}/> {'  '}
                {/* {this.props.item.rating} */}
              </Text>
            </Box>
      

          <Box flexDirection={'row'} justifyContent = {'space-between'}>
            
            <Box>
              <Pressable onPress={getDataPesawat}>
                <Text>Get Data</Text>
              </Pressable>
            </Box>
          </Box>

      </Box>


    </Pressable>       
    
  </Box>
);
}


export default ItemView