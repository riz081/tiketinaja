import React, { Component, useEffect, useState } from 'react';

import DropDownPicker from 'react-native-dropdown-picker';
import { Box, Pressable } from 'native-base'

import FontAwesome from 'react-native-vector-icons/FontAwesome';


const Drop =({ item }) => {
    const [dataPesawat, setDataPesawat] = useState({
        iata : '',
        shortName : '',
        })

    const optionsPesawat = {
        method: 'GET',
        headers: {
            'X-RapidAPI-Key': '0df8b82465msh5ec435d63e5d37ep1101f9jsnb1e132289fdc',
            'X-RapidAPI-Host': 'aerodatabox.p.rapidapi.com'
        }
        };


    const getDataPesawat = () => {
        fetch('https://aerodatabox.p.rapidapi.com/airports/icao/WIHH', optionsPesawat)
        .then(response => response.json())
        .then(json => {
            console.log(json);
            setDataPesawat(json)
        })
        .catch(err => console.error(err));
        }

    const [open, setOpen] = useState(false);
    const [value, setValue] = useState(null);
    const [items, setItems] = useState([
        {label: 'Kereta', value: 'Kereta', icon : () => <FontAwesome name='train' size={25} color = 'skyblue' />},
        {label: 'Pesawat',
         value: () => 'Pesawat',
         icon : () => <FontAwesome name='plane' size={25} color = 'skyblue' />}
    ]);


  useEffect(() => {
    console.log(value);
  }, [value])

  return (
    <Box flex={1} marginBottom = {'50px'} marginX={'20px'}>
        <DropDownPicker
            style = {{
                backgroundColor : '#fff'
            }}

            dropDownContainerStyle = {{
                backgroundColor : '#fff',
                borderColor : 'skyblue'
            }}

            textStyle = {{
                color : '#212121'
            }}

            open={open}
            value={value}
            items={items}
            setOpen={setOpen}
            setValue={setValue}
            setItems={setItems}
            />    
    </Box>
    
  );
}

export default Drop;