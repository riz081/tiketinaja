import React, { Component } from "react";
import { 
  Text, 
  Box, 
  Image,
  Pressable, 
  ScrollView} from "native-base"; 

import { StyleSheet, Dimensions } from 'react-native'
import BackgroundCurve from "../../components/BackgroundCurve";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Ionicons from 'react-native-vector-icons/Ionicons';
import AsyncStorage from '@react-native-async-storage/async-storage';

const W = Dimensions.get('window').width;

class DetailPesawat extends Component {
  constructor(props) {
    super(props);
    this.state = { 
      dataPesawat : this.props.route.params.dataPesawat,
      dataPesawatAPI : [],
     };
  }

  componentDidMount() {
      this.getData();
      this.dapatPesawat();
  }

  create = () => {
    let data = this.state.dataPesawat;
    
    this.setState({data});
    console.log('data Pesawat : ', data);

    this.saveData(data);
  }

  saveData = async (data) => {
    try {
      await AsyncStorage.setItem('@databasePesawat', JSON.stringify(data));
    }

    catch(e) {
      console.log('save eror', e);
    }
    this.setState({});
  }

  getData = async() => {
    try{
      let value = await AsyncStorage.getItem('@databasePesawat');
      value =JSON.parse(value);

      if(value !== null)
      {
        this.setState({data : value});
        console.log('data PesawatKu',value);
      }
    }
    catch (e){
      console.log('save eror', e);
    }
  }

  dapatPesawat = () => {
    fetch(
      'https://aerodatabox.p.rapidapi.com/airports/icao/WIHH',
       {
        "method" : 'GET',
        "headers" : {
          'X-RapidAPI-Key': '0df8b82465msh5ec435d63e5d37ep1101f9jsnb1e132289fdc',
              'X-RapidAPI-Host': 'aerodatabox.p.rapidapi.com'
        }
       })
    .then((response) => response.json())
    .then((json) => {
      this.setState({dataPesawatAPI: json});
      console.log(json);
    })
    .catch((error) => {
      console.error(error);
    })
  }

  render() {
    const { navigation } = this.props;
    const { dataPesawat, dataPesawatAPI } = this.state;
    return (
      
    <>

      {console.log(dataPesawat)}
      <Box
        flex={1}
        alignItems = 'center'
        background={'#F9F0F0'}
      >
        <Box
          width={'100%'}
          pt = {'10%'}
          justifyContent = 'space-between'
          flexDirection={'row'}
          alignItems = 'center'
          pl={'5%'}
          pr={'5%'}
        >
          <Pressable onPress={() => navigation.navigate('Watchlist')}>
            <FontAwesome
              name="chevron-left"
              size={20}
            />
          </Pressable>

          <Pressable onPress={() => navigation.navigate('Home')}>
            <AntDesign
              name="appstore-o"
              size={20}
            />
          </Pressable>
        </Box>

        <Image 
          source={{uri : 'https://images.unsplash.com/photo-1529074963764-98f45c47344b?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1786&q=80'}} 
          width = {'90%'}
          height = {(W - 10)/2 +70}
          resizeMode = 'contain'
          mt={10}
          borderRadius = {'20px'}
          mb={'20px'}
        />

        <ScrollView flex={1}>
          <Box
            width={'100%'}
            backgroundColor = '#fff'
            padding={'30px'}
            flex = {1}
            marginTop = {'40px'}
            mb={'40px'}
            borderTopLeftRadius = {'40px'}
            borderTopRightRadius = {'40px'}
          >
            <Box
              flexDirection={'row'}
              justifyContent = 'space-between'
              alignItems={'center'}
            >
              <Box mb = {'5px'}>
                <Text 
                  fontSize={'18px'}
                  fontWeight = 'extrabold'
                  color={'black'}
                  mb = {'5px'}>
                  {`Name : ${this.state.dataPesawat.name} \nBandara :  ${this.state.dataPesawatAPI == null ?  "Tunggu" : this.state.dataPesawatAPI.shortName}`}
                </Text>

                
              <Text
                fontSize={'15px'}
                fontWeight = 'bold'
                fontFamily={'heading'}           
              >
                <Ionicons name="time" size={20}/>
                {`Time: ${this.state.dataPesawat.time}`}
              </Text>

                <Text
                  fontSize={'15px'}
                  color = 'gray.500'
                  fontFamily={'body'}
                  top = {'15px'}
                >
                  {`Class: ${this.state.dataPesawat.class}`}
                </Text>

                
                <Text
                  fontSize={'15px'}
                  color = 'gray.500'
                  fontFamily={'body'}
                  top = {'10px'}
                  mt = {'15px'}
                  mb = {'5px'}
                >
                {`IATA : ${this.state.dataPesawatAPI == null ?  "Tunggu" : this.state.dataPesawatAPI.iata}`}
                </Text>

                <Text
                  fontSize={'15px'}
                  color = 'gray.500'
                  fontFamily={'body'}
                  top = {'10px'}
                >
                {`ICAO : ${this.state.dataPesawatAPI == null ?  "Tunggu" : this.state.dataPesawatAPI.icao}`}
                </Text>
                
              </Box>

            </Box>

            <Box
              flexDirection={'row'}
              justifyContent = 'space-between'
              top={'40px'}
              alignItems = 'center'
            >
              <Text
                fontWeight={'bold'}
                fontSize = {'27px'}
                fontFamily={'heading'}
              >
                {this.state.dataPesawat.price}
              </Text>
              <Pressable
                height={'50px'}
                width = {'70px'}
                backgroundColor = '#335AD7'
                borderRadius={'10px'}
                alignItems = 'center'
                justifyContent={'center'}

                
                onPress = {() => navigation.navigate('HistoryPesawat', {dataPesawat : dataPesawat}, () => this.create())}
              >
                <Text fontSize={'20px'} color = 'white' fontFamily={'head'}>
                  <FontAwesome name="cart-plus" size={30}/>
                  </Text>
              </Pressable>
            </Box>
          </Box>
        </ScrollView>
        
      </Box>

    </>
    );
  }
}

export default DetailPesawat;

const styles = StyleSheet.create({
  
  svg: {
    position: 'absolute',
    width: Dimensions.get('window').width,
    top: -170,
  },
});



