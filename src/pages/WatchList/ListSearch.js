import React, { Component, useState } from 'react';
import { Box, Text } from 'native-base';
import mocks from './mocks.json';
// import ItemView from './ItemView';

class ListSearch extends Component {
  
  render() {

    return (
      <Box 
        mt={'80px'}
        p = {'10px'}
        backgroundColor = '#fff'
      >        

        <Box paddingX={'15px'}>
          <Text
            fontSize={'16px'}
            fontWeight = 'bold'
            color={'#5f646a'}
          >Daftar List Tiket transportasi</Text>
        </Box>
        <Box mt={'15px'}>
          {mocks.map(item => {
            return <ItemView item={item} />;
          })}
          
        </Box>
      </Box>
    );
  }
}

export default ListSearch;
