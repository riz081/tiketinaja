import React, { Component } from "react";
import { 
  Text, 
  Box, 
  Image,
  Pressable } from "native-base"; 

import { StyleSheet, Dimensions } from 'react-native'
import BackgroundCurve from "../../components/BackgroundCurve";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Ionicons from 'react-native-vector-icons/Ionicons';
import AsyncStorage from '@react-native-async-storage/async-storage';

const W = Dimensions.get('window').width;

class detailKereta extends Component {
  constructor(props) {
    super(props);
    this.state = { 
      dataKereta : this.props.route.params.dataKereta
     };
  }

  componentDidMount() {
      this.getData();
  }

  create = () => {
    let data = this.state.dataKereta;
    
    this.setState({data});
    console.log('data Kereta : ', data);

    this.saveData(data);
  }

  saveData = async (data) => {
    try {
      await AsyncStorage.setItem('@databaseKereta', JSON.stringify(data));
    }

    catch(e) {
      console.log('save eror', e);
    }
    this.setState({});
  }

  getData = async() => {
    try{
      let value = await AsyncStorage.getItem('@databaseKereta');
      value =JSON.parse(value);

      if(value !== null)
      {
        this.setState({data : value});
        console.log('data KeretaKu',value);
      }
    }
    catch (e){
      console.log('save eror', e);
    }
  }

  render() {
    const { navigation } = this.props;
    const { dataKereta } = this.state;
    return (
      
    <>

      {/* {console.log(dataKereta)} */}
      <Box
        flex={1}
        alignItems = 'center'
        background={'#F9F0F0'}
      >
        <Box
          width={'100%'}
          pt = {'10%'}
          justifyContent = 'space-between'
          flexDirection={'row'}
          alignItems = 'center'
          pl={'5%'}
          pr={'5%'}
        >
          <Pressable onPress={() => navigation.navigate('Watchlist')}>
            <FontAwesome
              name="chevron-left"
              size={20}
            />
          </Pressable>

          <Pressable onPress={() => navigation.navigate('Home')}>
            <AntDesign
              name="appstore-o"
              size={20}
            />
          </Pressable>
        </Box>

        <Image 
          source={{uri : 'https://images.unsplash.com/photo-1474487548417-781cb71495f3?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1884&q=80'}} 
          width = {'100%'}
          height = {(W - 10)/2 +70}
          resizeMode = 'contain'
          mt={10}
          borderRadius = {'20px'}
        />

        <Box
          width={'100%'}
          backgroundColor = '#fff'
          padding={'30px'}
          flex = {1}
          marginTop = {'40px'}
          borderTopLeftRadius = {'40px'}
          borderTopRightRadius = {'40px'}
        >
          <Box
            flexDirection={'row'}
            justifyContent = 'space-between'
            alignItems={'center'}
          >
            <Box>
              <Text 
                fontSize={'18px'}
                fontWeight = 'extrabold'
                color={'black'}>
                {this.state.dataKereta.trainName}
              </Text>

              <Text
                fontSize={'15px'}
                color = 'gray.500'
                fontFamily={'body'}
                top = {'15px'}
              >
                {`Class: ${this.state.dataKereta.trainClass}`}
              </Text>

              <Text
                fontSize={'15px'}
                color = 'gray.500'
                fontFamily={'body'}
                top = {'10px'}
                mt = {'15px'}
                mb = {'25px'}
              >
                {`Route: ${this.state.dataKereta.trainOrigin.departureStation} - ${this.state.dataKereta.trainDestination.arrivalStation}`}
              </Text>
            </Box>

            <Text
              fontSize={'15px'}
              fontWeight = 'bold'
              fontFamily={'heading'}              
            >
              <Ionicons name="time" size={20}/>
              {`Time: ${this.state.dataKereta.trainOrigin.departureTime}`}
            </Text>
          </Box>

          <Box
            flexDirection={'row'}
            justifyContent = 'space-between'
            top={'40px'}
            alignItems = 'center'
          >
            <Text
              fontWeight={'bold'}
              fontSize = {'27px'}
              fontFamily={'heading'}
            >
              {this.state.dataKereta.ticketPrice}
            </Text>
            <Pressable
              height={'50px'}
              width = {'70px'}
              backgroundColor = '#335AD7'
              borderRadius={'10px'}
              alignItems = 'center'
              justifyContent={'center'}

              // onPress = {() => this.create()}
              onPress = {() => navigation.navigate('History', {dataKereta : dataKereta}, () => this.create())}
            >
              <Text fontSize={'20px'} color = 'white' fontFamily={'head'}>
                <FontAwesome name="cart-plus" size={30}/>
                </Text>
            </Pressable>
          </Box>
        </Box>
      </Box>

    </>
    );
  }
}

export default detailKereta;

const styles = StyleSheet.create({
  
  svg: {
    position: 'absolute',
    width: Dimensions.get('window').width,
    top: -170,
  },
});



