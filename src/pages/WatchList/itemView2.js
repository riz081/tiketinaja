import React, { Component, useState } from 'react';
import { Box, FlatList, Spinner, Pressable, Text } from 'native-base';
import mocks from './mocks.json';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';
import Fontisto from 'react-native-vector-icons/Fontisto';


class ItemView2 extends Component {
    constructor(props) {
      super(props);
      this.state = { 
        data : [],
        dataKereta : [],
        dataTaxi : [],
        isLoading : true,
        
       };
    }  
  
    getData = () => {
      fetch(
        'https://aerodatabox.p.rapidapi.com/airports/icao/WIHH',
         {
          "method" : 'GET',
          "headers" : {
            'X-RapidAPI-Key': '0df8b82465msh5ec435d63e5d37ep1101f9jsnb1e132289fdc',
                'X-RapidAPI-Host': 'aerodatabox.p.rapidapi.com'
          }
         })
      .then((response) => response.json())
      .then((json) => {
        this.setState({data: json});
        console.log(json);
      })
      .catch((error) => {
        console.error(error);
      })
    }
  
    getDataKereta = () => {
      fetch(
        'https://train-ticket-checker-indonesia.p.rapidapi.com/trains?from=PSE&to=YK&date=2023-01-31',
         {
          "method" : 'GET',
          "headers" : {
            'X-RapidAPI-Key': '0df8b82465msh5ec435d63e5d37ep1101f9jsnb1e132289fdc',
                'X-RapidAPI-Host': 'train-ticket-checker-indonesia.p.rapidapi.com'
          }
         })
      .then((response) => response.json())
      .then((json) => {
        this.setState({dataKereta: json});
        console.log(json);
      })
      .catch((error) => {
        console.error(error);
      })
    }
  
   
    componentDidMount = () => {
      this.getData();
      this.getDataKereta();
  }
  
  // renderItem = ({ item }) => {
  //   const { navigation } = this.props;
  
  //   return (
  //     <Box flexDirection={'row'} left={235} mt={'10px'}>
  //       <Pressable
  //         backgroundColor={'#157ffb'}
  //         width = {'80px'}
  //         height = {'30px'}
  //         justifyContent = 'center'
  //         alignItems={'center'}
  //         borderRadius = {'10px'}
  //         marginTop = {'10px'}
  
  //         onPress={() => navigation.navigate("ListDetail", { data: item })}
  //       >
  //         <Text fontWeight={'bold'} textTransform={'uppercase'} color='white'>
  //           <FontAwesome name='paper-plane-o' size={15} color="#fff"/> {'  '}
  //           go
  //         </Text>
  //       </Pressable>
  //     </Box>
  //   )
  // }
   
    render() {
  
      return (
  
        <Box>        
          {/* Data Kereta */}
          <FlatList
              flex={1}
              mt={'20px'}
              data={this.state.dataKereta}                    
              renderItem={({item, index}) => (
                <Box 
                  borderWidth={'1.5px'}
                  borderColor = {'#efeff0'}
                  mb = {'12px'}
                  p = {'20px'}
                  borderRadius = {'12px'}
                >                    
                    
                    <Text 
                      fontSize={'23px'}
                      fontWeight = 'bold'
                      color={'#585fcc'}
                      mb={'10px'}
                    >
                        {item.ticketPrice}              
                    </Text>
  
                    <Text 
                      fontSize={'15px'}
                      fontWeight = 'bold'
                      color={'#585fcc'}
                      mb={'10px'}
                    >
                        Type : {this.props.item.type1} 
                    </Text>
  
                      <Box flexDirection={'row'} justifyContent = 'space-between'>
                        <Text 
                          color={'#24333a'}
                          fontSize = {'14px'}
                          fontWeight = 'bold'
                        >
                          <FontAwesome name="train" size={20} /> {'  '}           
                          {item == null ?  "Tunggu" : item.trainName}
                        </Text>
  
                      </Box>
  
                      <Box flexDirection={'row'} justifyContent = 'space-between' mt={'10px'}>                      
  
                        <Text 
                          color={'#24333a'}
                          fontSize = {'14px'}
                          fontWeight = 'bold'
                        >
                          <FontAwesome name="user" size={20} /> {'  '}
                          {item == null ?  "Tunggu" : item.trainClass}
                        </Text>
  
                        <Text 
                          color={'#24333a'}
                          fontSize = {'14px'}
                          fontWeight = {'bold'}
                        >
                          <Ionicons name="time" size={20} /> {'  '}
                          {item.trainOrigin.departureTime}
                        </Text>
  
                        <Text 
                          color={'#24333a'}
                          fontSize = {'14px'}
                          fontWeight = {'bold'}
                        >
                          <MaterialCommunityIcons name="car-seat" size={20} /> {'  '}
                          {item.seatAvailability}
                        </Text>
                      </Box>                    
  
                    <Box flexDirection={'row'} justifyContent = {'space-between'}>
                      
                      <Box>
                        <Pressable onPress={() => this.getData()}>
                          <Text>Get Data</Text>
                        </Pressable>
  
                        <Pressable onPress={() => this.getDataKereta()}>
                          <Text>Get Data Kereta</Text>
                        </Pressable>
  
                        <Pressable onPress={() => this.getDataTaxi()}>
                          <Text>Get Data Taxi</Text>
                        </Pressable>
                      </Box>
                    </Box>
  
                </Box>
              )}
          />                            
          
          {/* Data Pesawat */}
             
          <Box 
            borderWidth={'1.5px'}
            borderColor = {'#efeff0'}
            mb = {'12px'}
            p = {'20px'}
            borderRadius = {'12px'}
          >                    
              
              <Text 
                fontSize={'23px'}
                fontWeight = 'bold'
                color={'#585fcc'}
                mb={'10px'}
              >
                  {this.props.item.harga}              
              </Text>
  
              <Text 
                  fontSize={'15px'}
                  fontWeight = 'bold'
                  color={'#585fcc'}
                  mb={'10px'}
                >
                  Type : {this.props.item.type2} 
                </Text>
  
                <Box flexDirection={'row'} justifyContent = 'space-between'>
                  <Text 
                    color={'#24333a'}
                    fontSize = {'14px'}
                    fontWeight = 'bold'
                  >
                    <FontAwesome name="plane" size={20}/> {'  '}
                    {this.state.data == null ?  "Tunggu" : this.state.data.iata}
                  </Text>
  
                  <Text 
                    color={'#24333a'}
                    fontSize = {'14px'}
                    fontWeight = 'bold'
                  >
                    <MaterialIcons name="local-airport" size={20}/> {'  '}
                    {this.state.data == null ?  "Tunggu" : this.state.data.shortName}
                  </Text>
  
                  <Text 
                    color={'#24333a'}
                    fontSize = {'14px'}
                    fontWeight = {'bold'}
                  >
                    <Ionicons name="ios-star" size={20}/> {'  '}
                    {this.props.item.rating}
                  </Text>
                </Box>
          
  
              <Box flexDirection={'row'} justifyContent = {'space-between'}>
                
                <Box>
                  <Pressable onPress={() => this.getData()}>
                    <Text>Get Data</Text>
                  </Pressable>
  
                  <Pressable onPress={() => this.getDataKereta()}>
                    <Text>Get Data Kereta</Text>
                  </Pressable>
                </Box>
              </Box>
  
          </Box>
  
        </Box>
   
      );
    }
  }


export default ItemView2