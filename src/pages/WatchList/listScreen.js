import React, { Component, useEffect, useState } from 'react';
import {
  StyleSheet,
  Dimensions,
} from 'react-native';
import BackgroundCurve from '../../components/BackgroundCurve';
import Entypo from 'react-native-vector-icons/Entypo';
// import BoxItem from './BoxItem';

import {useNavigation} from '@react-navigation/native';
import { Box, FlatList, Spinner, Pressable, Text, ScrollView, } from 'native-base';


import mocks from './mocks.json';
import ItemView from './ItemView';

import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';


import { Drop, BoxItem } from '../WatchList';


const styles = StyleSheet.create({
 
  svg: {
    position: 'absolute',
    width: Dimensions.get('window').width,
    top: -170,
  },
  
});


class WatchList extends Component {

  constructor(props) {
    super(props);
    this.state = {
      dataKereta : [],
      dataPesawat : [],
      dataPesawat2 : [
        {
          "price": "RP 4.159.000,-",
          "name": "Air Asia",
          "class": "Bisnis",
          "time": "Feb 2019",
          "seat": "Tersisa 2 kursi",
          "airline": "Cathay Pacific",
        },
        {
          "price": "Rp 5.199.000,-",
          "name": "Lion Air",
          "class": "Eksekutif",
          "time": "Feb 2019",
          "seat": "Tersisa 37 kursi",
          "airline": "American Pacific",          
        },
        {
          "price": "Rp 6.559.000,-",
          "name": "Qatar Airways",
          "class": "1St Class",
          "time": "Feb 2019",
          "seat": "Tersedia",
          "airline": "Jet Airways",
        },
        {
          "price": "RP 4.159.000,-",
          "name": "Air Asia",
          "class": "Ekonomi",
          "time": "Feb 2019",
          "seat": "Tersisa 59 kursi",
          "airline": "Cathay Pacific",
        },
        {
          "price": "Rp 6.559.000,-",
          "name": "Philipines Airways",
          "class": "1St Class",
          "time": "Feb 2019",
          "seat": "Tersisa 1 kursi",
          "airline": "Cathay Pacific",
        }
      ],
      isLoading : true,
    };

  }


  dapatPesawat = () => {
    fetch(
      'https://aerodatabox.p.rapidapi.com/airports/icao/WIHH',
       {
        "method" : 'GET',
        "headers" : {
          'X-RapidAPI-Key': '0df8b82465msh5ec435d63e5d37ep1101f9jsnb1e132289fdc',
              'X-RapidAPI-Host': 'aerodatabox.p.rapidapi.com'
        }
       })
    .then((response) => response.json())
    .then((json) => {
      this.setState({dataPesawat: json});
      console.log(json);
    })
    .catch((error) => {
      console.error(error);
    })
  }

  async dapatKereta() {
      try {
          const response = await fetch(
            'https://train-ticket-checker-indonesia.p.rapidapi.com/trains?from=PSE&to=YK&date=2023-02-20',
              {
              "method" : 'GET',
              "headers" : {
                'X-RapidAPI-Key': '0df8b82465msh5ec435d63e5d37ep1101f9jsnb1e132289fdc',
                    'X-RapidAPI-Host': 'train-ticket-checker-indonesia.p.rapidapi.com'
              }
              })
          const json = await response.json ()
          this.setState({dataKereta : json})   
          console.log(json);         
      } catch (error) {
          console.error(error)
      } finally {
          this.setState({ isLoading : false })
      }
  }

  componentDidMount() {
    this.dapatKereta();
    // this.dapatPesawat();
  }

  renderItem = ({ item }) => {
    const { navigation } = this.props;

    return (
      
        <Pressable
          onPress={() => navigation.navigate("DetailKereta", { dataKereta: item })}
        >

          
            <Box 
              borderWidth={'1.5px'}
              borderColor = {'#efeff0'}
              mb = {'12px'}
              p = {'20px'}
              borderRadius = {'12px'}
            >                    
              
              <Text 
                fontSize={'23px'}
                fontWeight = 'bold'
                color={'#585fcc'}
                mb={'20px'}
              >
                  {item.ticketPrice}              
              </Text>

              <Box flexDirection={'row'} justifyContent = 'space-between'>
                <Text 
                  color={'#24333a'}
                  fontSize = {'14px'}
                  fontWeight = 'bold'
                >
                  <FontAwesome name="train" size={20} /> {'  '}           
                  {item == null ?  "Tunggu" : item.trainName}
                </Text>

              </Box>

              <Box flexDirection={'row'} justifyContent = 'space-between' mt={'20px'}>                      

                <Text 
                  color={'#24333a'}
                  fontSize = {'14px'}
                  fontWeight = 'bold'
                >
                  <FontAwesome name="user" size={20} /> {'  '}
                  {item == null ?  "Tunggu" : item.trainClass}
                </Text>

                <Text 
                  color={'#24333a'}
                  fontSize = {'14px'}
                  fontWeight = {'bold'}
                >
                  <Ionicons name="time" size={20} /> {'  '}
                  {item.trainOrigin.departureTime}
                </Text>

                <Text 
                  color={'#24333a'}
                  fontSize = {'14px'}
                  fontWeight = {'bold'}
                >
                  <MaterialCommunityIcons name="car-seat" size={20} /> {'  '}
                  {item.seatAvailability}
                </Text>
              </Box>                    

            </Box>

        </Pressable>
    )
  }

  renderPesawat = ({ item }) => {
    const { navigation } = this.props;

    return (
      <Pressable
          onPress={() => navigation.navigate("DetailPesawat", { dataPesawat: item })}
      >
        <Box 
            borderWidth={'1.5px'}
            borderColor = {'#efeff0'}
            mb = {'12px'}
            p = {'20px'}
            borderRadius = {'12px'}
          >                    
              
              <Text 
                fontSize={'23px'}
                fontWeight = 'bold'
                color={'#585fcc'}
                mb={'10px'}
              >
                {item.price}{' '}  
              </Text>              
                
              <Box flexDirection={'row'} justifyContent = 'space-between'>
                <Text 
                    color={'#24333a'}
                    fontSize = {'14px'}
                    fontWeight = 'bold'
                  >
                    <FontAwesome name="plane" size={20}/> {'  '}
                    {item.name}{' '}  
                </Text>

              </Box>

              <Box flexDirection={'row'} justifyContent = 'space-between' mt={'20px'}>                      

                <Text 
                    color={'#24333a'}
                    fontSize = {'14px'}
                    fontWeight = 'bold'
                  >
                    <FontAwesome name="user" size={20}/> {'  '}
                    {item.class}{' '}  
                  </Text>

                  <Text 
                    color={'#24333a'}
                    fontSize = {'14px'}
                    fontWeight = 'bold'
                  >
                    <MaterialCommunityIcons name="airplane-clock" size={20}/> {'  '}
                    {item.time}{' '}  
                  </Text>

                  <Text 
                    color={'#24333a'}
                    fontSize = {'14px'}
                    fontWeight = 'bold'
                  >
                    <MaterialCommunityIcons name="car-seat" size={20}/> {'  '}
                    {item.seat}{' '}  
                  </Text>
              </Box>    
        </Box>
      </Pressable>
    )
  }

  
  render () {
    const { dataPesawat2, dataKereta, isLoading} = this.state;
    const { navigation } = this.props;
    
    const ItemPesawat = ({navigation, item}) => {
      return (        
          <Box 
            borderWidth={'1.5px'}
            borderColor = {'#efeff0'}
            mb = {'12px'}
            p = {'20px'}
            borderRadius = {'12px'}
          >                    
              
              <Text 
                fontSize={'23px'}
                fontWeight = 'bold'
                color={'#585fcc'}
                mb={'10px'}
              >
                {item.price}{' '}  
              </Text>              
                
              <Box flexDirection={'row'} justifyContent = 'space-between'>
                <Text 
                    color={'#24333a'}
                    fontSize = {'14px'}
                    fontWeight = 'bold'
                  >
                    <FontAwesome name="plane" size={20}/> {'  '}
                    {item.name}{' '}  
                </Text>

                <Text 
                    color={'#24333a'}
                    fontSize = {'14px'}
                    fontWeight = 'bold'
                  >
                    <MaterialIcons name="local-airport" size={20}/> {'  '}
                    {this.state.dataPesawat == null ?  "Tunggu" : this.state.dataPesawat.shortName}
                </Text>

              </Box>

              <Box flexDirection={'row'} justifyContent = 'space-between' mt={'20px'}>                      

                <Text 
                    color={'#24333a'}
                    fontSize = {'14px'}
                    fontWeight = 'bold'
                  >
                    <FontAwesome name="user" size={20}/> {'  '}
                    {item.class}{' '}  
                  </Text>

                  <Text 
                    color={'#24333a'}
                    fontSize = {'14px'}
                    fontWeight = 'bold'
                  >
                    <MaterialCommunityIcons name="airplane-clock" size={20}/> {'  '}
                    {item.time}{' '}  
                  </Text>

                  <Text 
                    color={'#24333a'}
                    fontSize = {'14px'}
                    fontWeight = 'bold'
                  >
                    <MaterialCommunityIcons name="car-seat" size={20}/> {'  '}
                    {item.seat}{' '}  
                  </Text>
              </Box>    
          </Box>
      )
    }   

    return (
     
        <Box 
          flex={1}
          position = {'relative'}
          backgroundColor = 'white'
        >
          <BackgroundCurve style={styles.svg}/>
          <ScrollView 
            marginTop={'22px'}
            flex = {1}
          >
            <Box
              flexDirection={'row'}
              alignItems = 'center'
            >
              <Pressable onPress={() => navigation.navigate('Home')}>
                <Entypo name='chevron-left' color={'#fff'} size={30}/>
              </Pressable>
              <Text>Search Result</Text>
            </Box>
  
            <BoxItem/>            

              {isLoading ? (
                <Spinner size={'lg'} color = {'#AA0002'}/>
              ) : (
              
                <Box
                  mt={'80px'}
                  p={'10px'}
                  backgroundColor = '#fff'            
                >
                  <Drop />
                  <Box
                    paddingX={'15px'}
                  >
                    <Text
                      fontSize={'16px'}
                      fontWeight = 'bold'
                      color={'#5f646a'}
                    >
                      Daftar List Tiket transportasi
                    </Text>
                  </Box>
  
                  <Box>
                    {/* Data Peswat */}
                  {/* <Pressable
                    onPress={() => navigation.navigate('DetailPesawat', {dataPesawat : dataPesawat})}
                  >
                    
                    {mocks.map(item => {
                      return <ItemPesawat item={item} />;
                    })}
                  </Pressable> */}

                    <FlatList
                      data={dataPesawat2}
                      keyExtractor={({ link }, index) => link}
                      renderItem={this.renderPesawat}
                    />

                    <FlatList
                        data={dataKereta}
                        keyExtractor={({ link }, index) => link}
                        renderItem={this.renderItem}
                      />
                  </Box>
                </Box>
                  
              )}
          </ScrollView>
        </Box>
  
        
      )
  }
}

export default WatchList
