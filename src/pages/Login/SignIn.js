import React, {useState} from 'react';
import { StyleSheet, Keyboard, Alert, Dimensions} from 'react-native';
import {
  View,
  Text,
  Pressable,
  Input,
} from 'native-base'
import { SignInHeader, Inputan, BackgroundCurve } from '../../components';
import AsyncStorage from '@react-native-async-storage/async-storage';

const walpaper  = require('../../assets/images/bgLogin.png')


const SignIn = ({navigation}) => {
  
  const [inputs, setInputs] = useState({email : '', password : ''});
  const [errors, setErrors] = useState({});

  const handleChange = (value, input) => {
    setInputs(prevState => ({...prevState, [input]: value}));
  };

  const handleError = (errMessage, input) => {
    setErrors(prevState => ({...prevState, [input]: errMessage}));
  };

  // Melakukan Validasi
  const validate = () => {
    Keyboard.dismiss();
    let isValid = true;

    if (!inputs.email){
      handleError('Please input email', 'email');
      isValid = false;
    } else if (!inputs.email.match(/\S+@\S+\.\S+/)){
      handleError('Invalid email', 'email');
      isValid = false;
    }

    if (!inputs.password) {
      handleError('Please input password', 'password');
      isValid = false;
    } else if (inputs.password.length < 5){
      handleError('Password must more than 5', 'password');
      isValid = false;
    }

    if (isValid) {
      login();
    }
  }

  const login = () => {
    setTimeout(async () => {
      let data = await AsyncStorage.getItem('userData');
      if (data) {
        let userData = JSON.parse(data);
        if(inputs.email == userData.email && inputs.password == userData.password){
          navigation.navigate('MainApp')
          AsyncStorage.setItem('userData', JSON.stringify({...userData, loggidIn: true}),
          );
        } else {
          Alert.alert('Error', 'User data invalid')
        }
      } else {
        Alert.alert('Error', 'User data not found')
      }
    }, 3000)    
  }

  
  return (
    
    <View flex={1}>
      {/* <ImageBackground source={walpaper} resizeMode='cover' style={{flex : 1, justifyContent : 'center'}}> */}        
        <BackgroundCurve style={styles.svg} />        
        <SignInHeader title="Login" />

        <Text
          textAlign={'center'}
          justifyContent = 'center'
          fontWeight={'bold'}
          fontSize = {16}
          marginBottom = {5}
        >
          Silahkan Melakukan Login
        </Text>

        <Text
          fontSize={16}
          fontWeight = {'600'}
          color = '#000'
          ml={5}
          mt = {2}          
        >
          Email
        </Text>

        <Inputan
          placeholder = "Masukkan Email"
          error= {errors.email}
          onFocus={() => {handleError(null, 'email')}}
          onChangeText = {value => handleChange(value, 'email')}                              
        />            

        <Text
          fontSize={16}
          fontWeight = '600'
          color={'#000'}
          ml={5}
          mt={2}          
        >Password</Text>

        <Inputan 
          placeholder = "Masukkan Password"
          error= {errors.password}
          onFocus={() => {handleError(null, 'password')}}
          onChangeText = {value => handleChange(value, 'password')}  
        />

        <Pressable
          mt={5} mr={5}
          onPress={() => navigation.navigate('LupaPassword')}>
          <Text textAlign={'right'} fontWeight='bold' color={'white'}>
            Lupa Password?
          </Text>
        </Pressable>

        <Pressable
          backgroundColor = '#03a9f4'
          paddingY={4}
          marginX = {5}
          justifyContent = 'center'
          alignItems={'center'}
          borderRadius = {'9px'}
          
          onPress = { validate } 
          >
          <Text color={'white'} fontSize = {'18px'} fontWeight = 'bold'>
            Sign In
          </Text>
        </Pressable>

        <Pressable mt={5} mr={5}          
          onPress={() => navigation.navigate('Register')}>
          <Text 
            textAlign={'center'} fontWeight={'bold'} color = 'black'
          >
            Bukan Member?{' '}
            <Text 
              color={'#03a9f4'}
            >Registrasi Disini</Text>
          </Text>
        </Pressable>
      {/* </ImageBackground> */}
    </View>
    
  );
}

export default SignIn;

const styles = StyleSheet.create({
  svg: {
    position: 'absolute',
    width: Dimensions.get('window').width,
  },  
})

