import React, {Component} from 'react';
import { View, ScrollView } from 'native-base'

import {
  Header,
  LoginRegisterButton,
  Title
} from '../../components'

class LoginLanding extends Component {
  
  render() {
    return (
      <ScrollView>
        <View 
          flex={1}
          backgroundColor = '#f7f6fd'
        >
          <Header />
          <Title />
          <LoginRegisterButton />
        </View>
      </ScrollView>    
    );
  }
}




export default LoginLanding;
