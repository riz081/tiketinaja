import React from 'react'
import {
    Text,
    Box,
    Pressable
} from 'native-base'

import AsyncStorage from '@react-native-async-storage/async-storage'

const MainPage = ({navigation}) => {

  return (
    <Box>
        <Text>
            Tes Login!!!
        </Text>
        <Pressable
            backgroundColor={'#7de24e'}
            borderWidth = {0}
            color = '#fff'
            borderColor={'#7de24e'}
            height = {'40px'}
            alignItems = 'center'
            borderRadius={'30px'}
            marginLeft = {'35px'}
            marginRight = {'35px'}
            marginTop = {'20px'}
            marginBottom = {'25px'} 
             
            activeOpacity = {0.5}
            onPress = {() => {
                AsyncStorage.clear();
                navigation.replace('LoginLanding');
            }}    
        >

            <Text 
                color={'#fff'}
                paddingY = {'10px'}
                fontSize = {'16px'}
                fontWeight = 'bold'
            >
                LOGOUT
            </Text>
        </Pressable>
    </Box>
  )
}

export default MainPage
