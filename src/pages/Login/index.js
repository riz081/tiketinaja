import LoginLanding from "./LoginLanding";
import LupaPassword from "./LupaPassword";
import MainPage from "./MainScreen";
import Register from "./Register";
import SignIn from "./SignIn";

export { LoginLanding, LupaPassword, MainPage, Register, SignIn }