import React, {useState} from 'react';
import { Keyboard, Alert, StyleSheet, Dimensions} from 'react-native';
import { View, Text, Pressable, ScrollView } from 'native-base'
import { SignInHeader, Inputan, BackgroundCurve2 } from '../../components';
import {useNavigation} from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';

const Register = ({}) => {
  
  const [inputs, setInputs] = useState({
    email : '',
    password : '',
    repassword : '',
  });

  const [errors, setErrors] = useState({});

  const handleChange = (value, input) => {
    setInputs(prevState => ({...prevState, [input]: value}));
  };

  const handleError = (errMessage, input) => {
    setErrors(prevState => ({...prevState, [input]: errMessage}));
  };

  // Melakukan Validasi
  const validate = () => {
    Keyboard.dismiss();
    let isValid = true;

    if (!inputs.email){
      handleError('Please input email', 'email');
      isValid = false;
    } else if (!inputs.email.match(/\S+@\S+\.\S+/)){
      handleError('Invalid email', 'email');
      isValid = false;
    }

    if (!inputs.password) {
      handleError('Please input password', 'password');
      isValid = false;
    } else if (inputs.password.length < 5){
      handleError('Password must more than 5', 'password');
      isValid = false;
    }

    if (!inputs.repassword) {
      handleError('Please input repassword', 'repassword');
      isValid = false;
    } else if (inputs.repassword.length < 5){
      handleError('Repassword must more than 5', 'repassword');
      isValid = false;
    }

    if (isValid) {
      registrasi();
    }
  }

  const registrasi = () => {
    setTimeout(() => {
      try{
        AsyncStorage.setItem('userData', JSON.stringify(inputs));
        navigation.navigate('SignIn');
      }catch(e) {
        Alert.alert('Error', 'Something went wrong')
      }
    }, 3000)
  }

  const navigation = useNavigation();
  return (
    <View flex={1} backgroundColor = '#f7f6fd'>
      <BackgroundCurve2 style={styles.svg} />
      <SignInHeader
        title="Registrasi"
      />

      <Text
        textAlign={'center'}
        justifyContent = 'center'
        fontWeight={'bold'}
        fontSize = {18}
        marginBottom = {4}
        color = 'white'
      >
        Silahkan masukkan data diri anda
      </Text>

      <Inputan
          placeholder = "Masukkan Email"
          error= {errors.email}
          onFocus={() => {handleError(null, 'email')}}
          onChangeText = {value => handleChange(value, 'email')}                              
      />      

      <Inputan
        placeholder = "Masukkan Password"
        error= {errors.password}
        onFocus={() => {handleError(null, 'password')}}
        onChangeText = {value => handleChange(value, 'password')}
      />

      <Inputan
        placeholder = "Masukkan Re-Password"
        error= {errors.repassword}
        onFocus={() => {handleError(null, 'repassword')}}
        onChangeText = {value => handleChange(value, 'repassword')}
      />

      
      {/* <Pressable
        mr={5}
        mt = {3}
        mb = {4}
        onPress={() => navigation.navigate('LupaPassword')}>
        <Text 
          textAlign={'right'}
          fontWeight = 'bold'
        >
          Lupa Password?
        </Text>
      </Pressable> */}

      <Pressable
        mt={5}
        backgroundColor = '#03a9f4'
        paddingY={4}
        marginX = {5}
        justifyContent = 'center'
        alignItems={'center'}
        borderRadius = {'9px'}
        
        onPress = {validate}
        >
        <Text 
          color={'#fff'}
          fontSize = {18}
          fontWeight = 'bold'
        >
          Registrasi
        </Text>
      </Pressable>

      <Pressable
        mt={5}
        mr = {5}
        
        onPress={() => navigation.navigate('SignIn')}>
        <Text 
          textAlign={'center'}
          fontWeight = 'bold'
        >
          Sudah Member? <Text color={'#03a9f4'} >Login Disini</Text>
        </Text>
      </Pressable>
    </View>
  );
};

export default Register;

const styles = StyleSheet.create({
  svg: {
    position: 'absolute',
    width: Dimensions.get('window').width,
  },  
})