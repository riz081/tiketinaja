import React from 'react'
import { 
    SafeAreaView,    
    Dimensions,
    StyleSheet
} from 'react-native'
import {
  Image,
  FlatList,
  View,
  Text,
  StatusBar,
  Pressable
} from 'native-base'
import { Images } from '../../assets'


const {width, height} = Dimensions.get('window');

const COLORS = {primary: '#f3f3f3', consta: '#000'};

const slides = [
    { id : 1, title: "Welcome to Tiketin@ja", subtitle : 'Lets travel with us 😊', image: Images.boardAstro},
    { id : 2, title: "Easy to get the ticket", subtitle : 'Enjoy youre travel 😎' , image: Images.boardAstro2}
]

const Slide = ({item}) => {
    return (
      <View style={{alignItems: 'center'}}>
        <Image
          source={item?.image}
          height = {'75%'}
          width = {width}
          resizeMode='contain'          
        />
        <View>
          <Text 
            color='#000'
            fontSize={"24px"}
            fontWeight = 'bold'
            marginTop={'20px'}
            textAlign = 'center'
          >{item?.title}</Text>
          <Text 
            color= '#000'
            fontSize= {'18px'}
            marginTop= {'10px'}
            maxWidth= {'70%'}
            textAlign= 'center'
            lineHeight= {'23px'}
          >{item?.subtitle}</Text>
        </View>
      </View>
    );
  };

const onBoarding = ({navigation}) => {    

    const [currentSlideIndex, setCurrentSlideIndex] = React.useState(0);
    const ref = React.useRef();
    const updateCurrentSlideIndex = e => {
        const contentOffsetX = e.nativeEvent.contentOffset.x;
        const currentIndex = Math.round(contentOffsetX / width);
        setCurrentSlideIndex(currentIndex);
    }

    const goToNextSlide = () => {
        const nextSlideIndex = currentSlideIndex + 1;
        if (nextSlideIndex != slides.length) {
          const offset = nextSlideIndex * width;
          ref?.current.scrollToOffset({offset});
          setCurrentSlideIndex(currentSlideIndex + 1);
        }
    };
    
    const skip = () => {
        const lastSlideIndex = slides.length - 1;
        const offset = lastSlideIndex * width;
        ref?.current.scrollToOffset({offset});
        setCurrentSlideIndex(lastSlideIndex);
    };

    const Footer = () => {
        return (
          <View
            justifyContent= 'space-between'
            paddingY={"20px"}
            height={height * 0.25}
          >
            {/* Indicator container */}
            <View
              flexDirection={'row'}
              justifyContent = 'center'
              marginTop={'20px'}
            >
              {/* Render indicator */}
              {slides.map((_, index) => (
                <View
                  height={'2.5px'}
                  width = {'10px'}
                  backgroundColor = 'grey'
                  marginX={'3px'}
                  borderRadius = {'2px'}
                  key={index}                  
                  style={[
                    currentSlideIndex == index && {
                      backgroundColor: COLORS.consta,
                      width: 25,
                    },
                  ]}
                />
              ))}
            </View>
    
            {/* Render buttons */}
            <View mb={"20px"}>
              {currentSlideIndex == slides.length - 1 ? (
                <View height={"50px"}>
                  <Pressable
                    flex= {1}
                    height= {'50px'}
                    borderRadius= {'5px'}
                    backgroundColor = '#00C3FF'
                    justifyContent = 'center'
                    alignItems = 'center'
                    borderColor={'#fff'}
                    borderWidth={'1px'}
                    ml={'15px'}
                    mr={'15px'}

                    onPress={() => navigation.replace('LoginLanding')}>
                    <Text 
                      fontWeight={'bold'}
                      fontSize = {'15px'}
                      color = "#fff"
                    >
                      GET STARTED
                    </Text>
                  </Pressable>
                </View>
              ) : (
                <View flexDirection={'row'}>
                  <Pressable
                    activeOpacity={0.8}
                    flex= {1}
                    height= {'50px'}
                    ml={'15px'}
                    borderRadius= {'5px'}
                    backgroundColor = '#00C3FF'
                    justifyContent = 'center'
                    alignItems = 'center'
                    borderColor={'#fff'}
                    borderWidth={'1px'}
                    
                    onPress={skip}>
                    <Text
                      fontWeight={'bold'}
                      fontSize = {'15px'}
                      color = '#fff'
                    >
                      SKIP
                    </Text>
                  </Pressable>
                  
                  <View w={"15px"} />
                  <Pressable
                    activeOpacity={0.8}
                    onPress={goToNextSlide}
                    flex= {1}
                    height= {'50px'}
                    mr={'15px'}
                    borderRadius= {'5px'}
                    backgroundColor = '#00C3FF'
                    justifyContent = 'center'
                    alignItems = 'center'
                    borderColor={'#fff'}
                    borderWidth={'1px'}
                  >
                    <Text
                      fontWeight={'bold'}
                      fontSize = {'15px'}
                      color = '#fff'
                    >
                      NEXT
                    </Text>
                  </Pressable>
                </View>
              )}
            </View>
          </View>
        );
      };

      
      return (
        <SafeAreaView style={{flex: 1, backgroundColor: COLORS.primary}}>
          <StatusBar backgroundColor={COLORS.primary} />
          <FlatList
            ref={ref}
            onMomentumScrollEnd={updateCurrentSlideIndex}
            contentContainerStyle={{height: height * 0.75}}
            showsHorizontalScrollIndicator={false}
            horizontal
            data={slides}
            pagingEnabled
            renderItem={({item}) => <Slide item={item} />}
          />
          <Footer />
        </SafeAreaView>
      );

}

export default onBoarding

