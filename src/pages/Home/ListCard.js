import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
} from 'react-native';
import {
  Box,
  ScrollView,
  Image,
} from 'native-base'
import mocks from './mocks.json';
const W = Dimensions.get('window').width;


const Item = ({item}) => {
  //   console.log(item.name);
  return (
    <Box
      mt={'10px'}
      paddingX = {'20px'}
    >
      <Box
        flex={1}
        borderRadius = {'10px'}
        overflow = 'hidden'
      >
        <Image source={{uri: item.image}} 
          width = {(W - 15)/2}
          height = {(W - 10)/2 +30}
          backgroundColor = 'gray.200'
        />
      </Box>      
    </Box>
  );
};

const ListCard = () => {
  return (
    <Box mt={'100px'} paddingX = {'20px'}>
      
      <ScrollView horizontal>
        {mocks.map(item => {
          return <Item key={item.id} item={item} />;
        })}
      </ScrollView>
    </Box>
  );
};

export default ListCard;
