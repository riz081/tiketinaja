import React from 'react'
import { 
  StyleSheet, 
  View, 
  
  StatusBar,
  Dimensions,
  TextInput
} from 'react-native'
import { 
  Box,
  Image,
  ScrollView,
  Pressable,
  Text
 } from 'native-base'
import { Logo } from '../../assets'
import AsyncStorage from '@react-native-async-storage/async-storage'
import ListCard from './ListCard'
import BackgroundCurve from '../../components/BackgroundCurve'
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5'
import Feather from 'react-native-vector-icons/Feather';
import Ionicons from 'react-native-vector-icons/Ionicons';
import AntDesign from 'react-native-vector-icons/AntDesign';


const Home = ({navigation}) => {

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <Box
        flex={1}
        position = 'relative'
        backgroundColor={'#fff'}
      >
        <BackgroundCurve style={{position : 'absolute', width: Dimensions.get('window').width,}} />
        <ScrollView flex={1}>
          <Box 
            marginTop={'12px'}
            p={'15px'}
          >
            <Box
              flexDirection={'row'}
              justifyContent = 'space-between'
              alignItems={'center'}
            >

              <Box 
                flexDirection={'row'}
                marginLeft = {'10px'}
              >                
              </Box>

              <Box
                marginRight={'10px'}
              >
                <FontAwesome5Icon name="user-circle" color="#fff" size={26} />
              </Box>
            </Box>
            
            <Image source={Logo}              
              ml = {"105px"}
              width={"180px"}
              height = {"150px"}
            />

            <Text
              fontSize={'30px'}
              fontWeight = 'bold'
              color={'#fff'}
              textAlign = 'center'
              marginTop={'35px'}
            >{`Welcome to GitLab TiketinAja`}
            </Text>


            <Box 
              mt={'20px'}
              p={'10px'}
            >
              
              <Box flexDirection={'row'} alignSelf ='center'>
                <Pressable 
                  flexDirection={'row'}
                  backgroundColor = '#00c3ff'
                  p={'5px'}
                  marginRight = {'30px'}
                  paddingX = {'15px'}
                  borderRadius = {'20px'}
                  borderWidth = {'1px'}
                  borderColor = '#fff'

                  onPress={() => navigation.navigate('Watchlist')}
                >
                  <FontAwesome5Icon name="train" color="#fff" size={16} style={{marginLeft : 10}} />
                  <Text 
                    fontWeight={'500'}
                    color = '#fff'
                    marginLeft={'5px'}
                    marginRight = {'10px'}
                  >Train</Text>
                </Pressable>
                <Pressable
                  flexDirection={'row'}
                  backgroundColor = '#00c3ff'
                  p={'5px'}
                  
                  paddingX = {'15px'}
                  borderRadius = {'20px'}
                  borderWidth = {'1px'}
                  borderColor = '#fff'
                  style = {{backgroundColor : 'transparent'}}

                  onPress={() => navigation.navigate('Watchlist')}
                >
                  <Ionicons name="ios-airplane" color="#fff" size={16} style={{marginLeft : 10}} />
                  <Text 
                    fontWeight={'500'}
                    color = '#fff'
                    marginLeft={'5px'}
                    marginRight = {'10px'}
                  >Flights</Text>
                </Pressable>
                
              </Box>
            </Box>
          </Box>
          <ListCard />
        </ScrollView>
{/*   
        <Pressable 
            backgroundColor={'#00cfee'}
            borderWidth = {1}
            color = '#fff'
            borderColor={'#7de24e'}
            height = {'40px'}
            w = {'40px'}
            alignItems = 'center'
            borderRadius={'70px'}
            marginLeft = {'35px'}
            mt = {'10px'}
            mb = {'10px'}
            left = {320}

            activeOpacity = {0.5}
            onPress = {() => {
                AsyncStorage.clear();
                navigation.replace('LoginLanding');
            }}    
        >

            <AntDesign name="logout" color="#fff" size={26} style={{paddingVertical : 5}} />
          
        </Pressable> */}
  
      </Box>

      
    </>
  )
}

export default Home
