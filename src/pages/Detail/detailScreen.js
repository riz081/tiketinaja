import React, { Component } from 'react'
import { Text, Box, Center, Heading, Image, ScrollView, NativeBaseProvider, Pressable, FlatList} from "native-base"; 

import {
  Button,
  Modal,
  Animated,
} from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Entypo from 'react-native-vector-icons/Entypo';
import AntDesign from 'react-native-vector-icons/AntDesign';


import AsyncStorage from '@react-native-async-storage/async-storage';

const ModalPoup = ({visible, children}) => {
  const [showModal, setShowModal] = React.useState(visible);
  const scaleValue = React.useRef(new Animated.Value(0)).current;
  React.useEffect(() => {
    toggleModal();
  }, [visible]);
  const toggleModal = () => {
    if (visible) {
      setShowModal(true);
      Animated.spring(scaleValue, {
        toValue: 1,
        duration: 300,
        useNativeDriver: true,
      }).start();
    } else {
      setTimeout(() => setShowModal(false), 200);
      Animated.timing(scaleValue, {
        toValue: 0,
        duration: 300,
        useNativeDriver: true,
      }).start();
    }
  };
  return (
    <Modal transparent visible={showModal}>
      <Box
        flex={1}
        backgroundColor = 'rgba(0,0,0,0.5)'
        justifyContent={'center'}
        alignItems = 'center'
      >
        <Animated.View
          style={[{
            width: '80%',
            backgroundColor: 'white',
            paddingHorizontal: 20,
            paddingVertical: 30,
            borderRadius: 20,
            elevation: 20,
          }, {transform: [{scale: scaleValue}]}]}>
          {children}
        </Animated.View>
      </Box>
    </Modal>
  );
};

class Detail extends Component {
  constructor(props) {
    super(props);
    this.state = { 
      dataKereta : [this.props.route.params.dataKereta],
      index : '',
      visible : false
     };
     this.setVisible = this.setVisible.bind(this);
     this.closeVisible = this.closeVisible.bind(this);
  }

  componentDidMount() {
    this.getData();
    this.create();
}

create = () => {
  let data = this.state.dataKereta;
  
  this.setState({data});
  console.log('data Kereta : ', data);

  this.saveData(data);
}

delete = () => {
  const { navigation } = this.props;
  let data = this.state.dataKereta;
  alert('Transaksi dibatalkan ?')

  data.splice(this.state.index, 1);

  this.setState({data});
  this.saveData(data);
  navigation.navigate('Watchlist')
}

saveData = async (data) => {
  try {
    await AsyncStorage.setItem('@databaseKereta', JSON.stringify(data));
  }

  catch(e) {
    console.log('save eror', e);
  }
  this.setState({});
}

getData = async() => {
  try{
    let value = await AsyncStorage.getItem('@databaseKereta');
    value =JSON.parse(value);

    if(value !== null)
    {
      this.setState({data : value});
      console.log('data KeretaKu',value);
    }
  }
  catch (e){
    console.log('save eror', e);
  }
}

setVisible() {
  this.setState({ visible : true })
}

closeVisible() {
  this.setState({ visible : false })
}

renderItem = ({ item }) => {
  const { navigation } = this.props;
  const { index, visible } = this.state;
  // const [visible, setVisible] = React.useState(false);

  return (

      <Box flex={1} alignItems = 'center' background={'#f9f0f0'}>
      
      <Box
          width={'100%'}
          pt = {'10%'}
          justifyContent = 'space-between'
          flexDirection={'row'}
          alignItems = 'center'
          pl={'5%'}
          pr={'5%'}
        >
          <Pressable onPress={() => navigation.navigate('DetailKereta')}>
            <FontAwesome
              name="chevron-left"
              size={20}
            />
          </Pressable>

          <Text fontSize={'20px'} fontWeight = 'bold' fontFamily={'heading'} textAlign = 'center'>Wishlist Payment</Text>

          <Pressable onPress={() => navigation.navigate('Home')}>
            <AntDesign
              name="appstore-o"
              size={20}
            />
          </Pressable>
      </Box>
      
      <Pressable>

        <Box 
          borderWidth={'1.5px'}
          borderColor = {'#efeff0'}
          mb = {'12px'}
          p = {'20px'}
          borderRadius = {'12px'}
        >   

          
          <Text 
            fontSize={'23px'}
            fontWeight = 'bold'
            color={'#585fcc'}
            mb={'20px'}
          >
              {item.ticketPrice}              
          </Text>

          <Box flexDirection={'row'} justifyContent = 'space-between'>
            <Text 
              color={'#24333a'}
              fontSize = {'14px'}
              fontWeight = 'bold'
            >
              <FontAwesome name="train" size={20} /> {'  '}           
              {item == null ?  "Tunggu" : item.trainName}
            </Text>

          </Box>

          <Box flexDirection={'row'} justifyContent = 'space-between' mt={'20px'}>                      

            <Text 
              color={'#24333a'}
              fontSize = {'14px'}
              fontWeight = 'bold'
            >
              <FontAwesome name="user" size={20} /> {'  '}
              {item == null ?  "Tunggu" : item.trainClass}
            </Text>

            <Text 
              color={'#24333a'}
              fontSize = {'14px'}
              fontWeight = {'bold'}
            >
              <Ionicons name="time" size={20} /> {'  '}
              {item.trainOrigin.departureTime}
            </Text>

            <Text 
              color={'#24333a'}
              fontSize = {'14px'}
              fontWeight = {'bold'}
            >
              <MaterialCommunityIcons name="car-seat" size={20} /> {'  '}
              {item.seatAvailability}
            </Text>
          </Box> 

          <Box flexDirection={'row'} marginTop={'15px'}>
            <Pressable
              backgroundColor={'crimson'}
              paddingX = {'10px'}
              height={'40px'}
              width = {'45px'}
              alignItems = 'center'
              justifyContent={'center'}
              borderRadius = {'5px'}

              onPress = {() => this.setState({index}, () => this.delete())}
            >
              <Box flex={1} mt={'7px'}>
                <Text color={'white'}>
                  <Entypo name='trash' size={25} color = 'white'/>
                </Text>
              </Box>
            </Pressable>  

            <ModalPoup visible={visible}>
              <Box alignItems={'center'}>
                <Box
                  width={'100%'}
                  height = {'40px'}
                  alignItems = 'flex-end'
                  justifyContent={'center'}
                >
                  <Pressable
                    onPress={this.closeVisible}
                  >
                    <FontAwesome name='close' size={25}/>
                  </Pressable>
                </Box>
              </Box>  
              <Box alignItems={'center'}>
                <FontAwesome name='check-circle' size={150} style = {{marginVertical : 10}}/>
              </Box>
              <Text marginY={'30px'} fontSize = {'20px'} textAlign = 'center'>
                  Transaksi Berhasil !
              </Text>

              <Box flexDirection={'row'} alignItems = 'center' justifyContent={'center'}>
                <Pressable
                  height={'30px'}
                  width = {'40px'}
                  backgroundColor = '#335AD7'
                  borderRadius={'10px'}
                  alignItems = 'center'
                  justifyContent={'center'}

                  onPress = {() => navigation.navigate('Watchlist')}
                >
                  <Text fontSize={'15px'} color = 'white' fontFamily={'head'}>
                    OK
                  </Text>
                </Pressable>
              </Box>
              
            </ModalPoup>              
            <Pressable 
              backgroundColor={'#32a852'}
              paddingX = {'10px'}
              height={'40px'}
              width = {'45px'}
              alignItems = 'center'
              justifyContent={'center'}
              borderRadius = {'5px'}
              ml={'10px'}

              onPress={this.setVisible}> 
              <FontAwesome5 name='money-bill-wave-alt' size={25} color = 'white' />
            </Pressable>
          </Box>                   

        </Box>

      </Pressable>

      </Box>
  )
}


  render() {
    const { dataKereta } = this.state;
    
    return (

      <>
        {/* {console.log(dataKereta)} */}
        
          {/* <Pressable
                height={'50px'}
                width = {'70px'}
                backgroundColor = '#335AD7'
                borderRadius={'10px'}
                alignItems = 'center'
                justifyContent={'center'}

                onPress = {() => this.create()}
              >
                <Text fontSize={'20px'} color = 'white' fontFamily={'head'}>
                  <FontAwesome name="cart-plus" size={30}/>
                  </Text>
          </Pressable> */}

          <Text> {dataKereta.ticketPrice}</Text>

          <Box marginTop={'15px'}>
            <FlatList
              data={dataKereta}
              keyExtractor={({ link }, index) => link}
              renderItem={this.renderItem}
            />
          </Box>
      </>
    );
  }
}


export default Detail
