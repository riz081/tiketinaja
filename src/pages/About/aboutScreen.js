import React, { Component } from "react";
import {
  Text,
  ScrollView,
  Image,
  Pressable,
  Box
} from 'native-base';
import { Logo } from "../../assets";
import BackgroundCurve from '../../components/BackgroundCurve'
import { StyleSheet, Dimensions } from "react-native";
import AsyncStorage from '@react-native-async-storage/async-storage'
import AntDesign from 'react-native-vector-icons/AntDesign';


class About extends Component {
  
  render() {
    const { navigation } = this.props;
    return (      

      <Box flex={1} position = "relative" color={"fff"}>
        <BackgroundCurve style={styles.svg} />
        <ScrollView
          flex={1}
          padding={"30px"}
        >
          

          <Image source={Logo}
            width={"200px"}
            height={"250px"}
            resizeMode="contain"
            alignSelf="center"
          />     

          <Text fontSize={14} fontWeight = "bold" mt={4}>
            Tiket@aja merupakan salah satu penyedia jasa layanan pemesanan tiket transportasi umum seperti pesawat, kereta api dan taxi secara online.  
          </Text>   

          <Text fontSize={14} fontWeight = "bold" mt={8}>
          Tim Developer : Rizqika Ibnu, Wahyu Aji, Naufal Rakhmad, Frisca Husniah, dan Sherly Agustin
          </Text>   

          <Text fontSize={14} fontWeight = "bold" mt={8}>
          RapidApi : Train Ticket Checker - Indonesia, Aerodatabox
          </Text>   

          <Text fontSize={20} fontWeight = "bold" mt={4} textAlign="center">
            Version 1.0.0
          </Text> 

        </ScrollView>

        
        <Pressable 
            backgroundColor={'#00cfee'}
            borderWidth = {1}
            color = '#fff'
            borderColor={'#7de24e'}
            height = {'40px'}
            w = {'40px'}
            alignItems = 'center'
            borderRadius={'70px'}
            marginLeft = {'35px'}
            mt = {'10px'}
            mb = {'10px'}


            activeOpacity = {0.5}
            onPress = {() => {
                AsyncStorage.clear();
                navigation.replace('LoginLanding');
            }}    
        >

            <AntDesign name="logout" color="#fff" size={26} style={{paddingVertical : 5}} />
          
          </Pressable>
      </Box>
      
      

    );
  }
}

export default About;

const styles = StyleSheet.create({
  svg: {
    position: 'absolute',
    width: Dimensions.get('window').width,
  },
})