import React, { Component } from 'react';
import {Dimensions} from 'react-native';
import { View, StatusBar, Image } from 'native-base'

const width = Dimensions.get("window").width;

class Header extends Component {
  
  render() {    

    return (
      <View>
        <StatusBar backgroundColor={'#f7f6fd'} barStyle="dark-content" />
        <View
          justifyContent={'center'}
          alignItems = 'center'
          marginTop={"20px"}
        >
          <Image
            source={require('../assets/images/astro.png')} width={width - 40} height={width-40} borderRadius={"25px"}            
          />
        </View>
      </View>
    );
  }
}


export default Header;
