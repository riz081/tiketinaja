import React, { Component } from 'react'
import { StyleSheet, Text, Pressable } from 'native-base'


class Button extends Component {
  
  render() {
    return (
      <Pressable 
        mt={6}
        height = {'55px'}
        width = {'100%'}
        backgroundColor = '#ffc103'
        borderRadius={'15px'}
        justifyContent = 'center'
        alignItems={'center'}
      >
          <Text 
            fontSize={18}
            fontWeight = {'600'}
            color = 'black'
          >Sign In</Text>
      </Pressable>
    );
  }
}


export default Button
