import React, { Component } from 'react';
import {View, Text} from 'native-base';

class Title extends Component {
  state = {  }
  render() {
    return (
      <View
        justifyContent={'center'}
        alignItems = 'center'
        marginTop={'20px'}
      >
        <Text
          fontSize={'28px'}
          fontWeight='bold'
          color={'#373248'}
          textAlign='center'
        >
         Tiketin@ja
        </Text>
        <Text textAlign={'center'} color='black'>
          Be Your Best Self, Strive Your Dream
        </Text>
        <Text 
          textAlign = 'center'
          marginY={'35px'}
          color='black'
          margin={'15px'}
        >
          Tiket@aja merupakan salah satu penyedia jasa layanan pemesanan tiket
           transportasi umum seperti pesawat, kereta api dan taxi secara online. 
        </Text>
      </View>
    );
  }
}


export default Title;
