import React, { useState } from 'react'
import { TextInput, StyleSheet } from 'react-native';
import {    
    View,
    Text,
    Pressable,
  } from 'native-base'

const Input = ({label, password, error,onFocus, ...props}) => {

    const [isFocus, setIsFocus] = useState(false);
    const [hidePassword, setHidePassword] = useState(password);

    return (
        <View mb={3}>
            <Text
                fontSize={14}
                color = '#3c3737'
            >{label}</Text>

            <View
                marginX={5}
                backgroundColor = '#fff'
                borderRadius={'9px'}
                pl = {'10px'}
                flexDirection={'row'}
                alignItems = 'center'

                
            >
                <TextInput
                    secureTextEntry = {hidePassword}
                    {...props}
                    style={styles.textInput}
                    onFocus={() => {
                        onFocus();
                        setIsFocus(true);
                    }}
                    onBlur={() => {
                        setIsFocus(false);
                    }}
                />

                {(
                    <Pressable                        
                        mr={3}
                        onPress={() => {
                            setHidePassword(!hidePassword);
                        }}
                    >
                        {hidePassword ? (
                            <Text 
                                fontSize = {12}
                                color = '#000'
                                fontWeight={'600'}
                            >
                                Show
                            </Text>
                        ) : (
                            <Text 
                                fontSize = {12}
                                color = '#000'
                                fontWeight={'600'}
                            >Hide</Text>
                        )}
                    </Pressable>
                )}
            </View>

            {error && <Text style={{fontSize : 14, color : 'red', fontWeight : 'bold', marginLeft : 25, marginTop : 10}}>
                {error}
            </Text>}
        </View>   
    )
}

export default Input

const styles = StyleSheet.create({
    textInput : {
        marginLeft : 10,
        color : '#000',
        flex : 1
    }
})