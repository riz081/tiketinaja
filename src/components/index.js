import Header from "./Header";
import LoginRegisterButton from "./LoginRegisterButton";
import Title from "./Title";
import SignInHeader from "./SignInHeader";
import Inputan from "./Input";
import Button from "./Button";
import BackgroundCurve from "./BackgroundCurve";
import BackgroundCurve2 from "./BackgroundCurve2";

export { Header, LoginRegisterButton, Title, SignInHeader, Inputan, Button, BackgroundCurve, BackgroundCurve2 }