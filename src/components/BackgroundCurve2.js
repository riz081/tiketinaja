import React from 'react';
import { View } from 'native-base';
import { StyleSheet } from 'react-native';
import Svg, { Path } from 'react-native-svg';

const BackgroundCurve2 = ({style}) => {
  return (
    <View style={style}>

        <View 
            backgroundColor={'#00C3FF'}
            height = {'350px'}
        />        

        <Svg height="50%" width="100%" style={styles.svg} viewBox="0 0 1440 320">
            <Path
            fill="#00C3FF"
            d="M0,256L48,261.3C96,267,192,277,288,245.3C384,213,480,139,576,101.3C672,64,768,64,864,101.3C960,139,1056,213,1152,218.7C1248,224,1344,160,1392,128L1440,96L1440,0L1392,0C1344,0,1248,0,1152,0C1056,0,960,0,864,0C768,0,672,0,576,0C480,0,384,0,288,0C192,0,96,0,48,0L0,0Z"
            />
        </Svg>
      
        <Svg height="190%" width="100%" style={styles.svg} viewBox="0 0 1440 320">
            <Path
            fill="#00C3FF"
            d="M0,160L0,160L96,160L96,192L192,192L192,0L288,0L288,128L384,128L384,224L480,224L480,32L576,32L576,224L672,224L672,160L768,160L768,160L864,160L864,256L960,256L960,32L1056,32L1056,96L1152,96L1152,192L1248,192L1248,224L1344,224L1344,256L1440,256L1440,320L1344,320L1344,320L1248,320L1248,320L1152,320L1152,320L1056,320L1056,320L960,320L960,320L864,320L864,320L768,320L768,320L672,320L672,320L576,320L576,320L480,320L480,320L384,320L384,320L288,320L288,320L192,320L192,320L96,320L96,320L0,320L0,320Z"
            />
        </Svg>         

    </View>
  );
};

export default BackgroundCurve2;

const styles = StyleSheet.create({
  svg: {
    position: 'absolute',
    top: 300
 },
});
