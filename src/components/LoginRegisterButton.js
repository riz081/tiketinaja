import React from 'react';
// import {View, Text, Pressable} from 'react-native';
import {
  View,
  Text,
  Pressable
} from 'native-base'


import {useNavigation} from '@react-navigation/native';

const LoginRegisterButton = () => {
  const navigation = useNavigation();
  return (
    <View
      flexDirection={'row'}
      backgroundColor = '#f1f3ff'
      marginX={5}
      borderRadius = {4}
      borderWidth = {'2px'}
      borderColor = '#fff'
      marginBottom={5}
    >
      <Pressable
        flex={1}
        justifyContent = 'center'
        alignItems={'center'}
        backgroundColor = '#fff'
        paddingY={5}
        borderRadius = {4}
        
        onPress={() => navigation.navigate('Register')}>
        <Text fontWeight={'bold'} color='black'>Register</Text>
      </Pressable>
      <Pressable
        flex={1}
        justifyContent = 'center'
        alignItems={'center'}
        backgroundColor = '#f1f3ff'
        paddingY={5}
        borderTopRadius = {4}
        borderBottomRadius = {4}
        
        onPress={() => navigation.navigate('SignIn')}>
        <Text fontWeight={'bold'} color='black'>Sign In</Text>
      </Pressable>
    </View>
  );
};

export default LoginRegisterButton;
