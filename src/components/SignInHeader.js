import React from 'react';
import {View, Text} from 'native-base';

const SignInHeader = props => {
  return (
    <View mt={'55px'}>
      <Text
        fontSize={'40px'}
        fontWeight = 'bold'
        color={'#d9d9d9'}
        textAlign = 'center'
      >
        {props.title}
      </Text>
      <Text
        fontSize={'16px'}
        textAlign = 'center'
      >
        {props.description}
      </Text>
    </View>
  );
};

export default SignInHeader;
