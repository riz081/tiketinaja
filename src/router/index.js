import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { 
    Splash,
    About,
    Home,
    Detail, 
    WatchList,
    LoginLanding,
    SignIn,
    LupaPassword,
    Register,
    onBoarding,
    ItemView,
    DetailKereta,
    DetailPesawat,
    historyPesawat
  } 
from '../pages';
import Entypo from 'react-native-vector-icons/Entypo'


const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

const screenOptions = ({route}) => ({
    tabBarIcon: ({focused, color, size}) => {
      let iconName;

      if (route.name === 'Home') {
        iconName = 'home';
      } else if (route.name === 'Watchlist') {
        iconName = 'ticket';
      } else if (route.name === 'History') {
        iconName = 'list';
      } else if (route.name === 'About') {
        iconName = 'info-with-circle';
      }
      

      // You can return any component that you like here!
      return <Entypo name={iconName} size={size} color={color} />;
    },
    headerShown : false,
  });

const MainApp = () => {
    return (
        <Tab.Navigator
            screenOptions={screenOptions}
            tabBarOptions={{
            activeTintColor: '#00C3FF',
            inactiveTintColor: '#464962',
          }}
        >
            <Tab.Screen name='Home' component={ Home } />
            <Tab.Screen name='Watchlist' component={ WatchList } />
            {/* <Tab.Screen name='History' component={ Detail } /> */}
            <Tab.Screen name='About' component={ About } />
        </Tab.Navigator>
    )
}

const Router = () => {
  return (
    <Stack.Navigator 
        initialRouteName='Splash' 
        screenOptions={{headerShown:false}}>
        <Stack.Screen name='Splash' component={ Splash } />
        <Stack.Screen name='onBoarding' component={ onBoarding } />
        <Stack.Screen name='MainApp' component={ MainApp } />
        <Stack.Screen name="DetailKereta" component={DetailKereta} />
        <Stack.Screen name="DetailPesawat" component={DetailPesawat} />
        {/* <Stack.Screen name="ItemView" component={ItemView} /> */}
        <Stack.Screen name='History' component={ Detail } />
        <Stack.Screen name='HistoryPesawat' component={ historyPesawat } />
        <Stack.Screen name="LoginLanding" component={LoginLanding} />
        <Stack.Screen name="SignIn" component={SignIn} />
        <Stack.Screen name="Register" component={Register} />
        {/* <Stack.Screen name="LupaPassword" component={LupaPassword} /> */}
    </Stack.Navigator>
  )
}

export default Router;
