import Logo from './logo.png';
import SplashBackground from './SplashBackground.png';
import AstroImg from './astro.png';


const Images = {
    boardAstro : require('./Board_1.png'),
    boardAstro2 : require ('./Board_2.png')
}


export { Logo, SplashBackground, AstroImg, Images }