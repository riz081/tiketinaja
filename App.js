import React from 'react'
import { NavigationContainer } from '@react-navigation/native';
import Router from './src/router';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { NativeBaseProvider } from 'native-base';


const App = () => {
  return (
    <SafeAreaProvider>
      <NativeBaseProvider>        
        <NavigationContainer>
          <Router />
        </NavigationContainer>
      </NativeBaseProvider>
    </SafeAreaProvider>
  )
}

export default App

